// index.js

const serverless = require("serverless-http");
const express = require("express");
const app = express();
const axios = require("axios");
var cors = require("cors");

app.use(cors());

app.get("/weather/:zipcode", (req, res, next) => {
  let zipcode = req.params.zipcode;
  axios
    .get(
      `http://api.openweathermap.org/data/2.5/weather?zip=${zipcode},us&appid=d03f35a38aeab15de86c26547d276a44`
    )
    .then(response => {
      console.log(response.data.weather[0]);
      res
        .json(response.data.weather[0])
        .header("Access-Control-Allow-Origin", "*");
    })
    .catch(error => {
      console.log("Oh No! Error!");
      console.log(error);
    });
});

module.exports.handler = serverless(app);
