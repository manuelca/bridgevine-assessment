import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HttpClientModule } from "@angular/common/http";
import { TranslateModule } from "@ngx-translate/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { CoreModule } from "@app/core";
import { SharedModule } from "@app/shared";
import { ShellModule } from "./shell/shell.module";
import { HomeModule } from "./home/home.module";
import { WeatherModule } from "./weather/weather.module";
import { HttpModule } from "@angular/http";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
// import { WeatherComponent } from "./weather/weather.component";

@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    CoreModule,
    SharedModule,
    WeatherModule,
    ShellModule,
    HomeModule,
    // AboutModule,
    AppRoutingModule // must be imported as the last module as it contains the fallback route
  ],
  declarations: [AppComponent], //, WeatherComponent
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
