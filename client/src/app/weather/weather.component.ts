import { Component, OnInit } from "@angular/core";
import { Http, Response } from "@angular/http";
import { NgModule } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { WeatherReqService } from "../weatherS/weather-req.service";

@Component({
  selector: "app-weather",
  templateUrl: "./weather.component.html",
  providers: [WeatherReqService],
  styleUrls: ["./weather.component.scss"]
})
export class WeatherComponent implements OnInit {
  weatherReq: any;

  constructor(private weatheservice: WeatherReqService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.submitForm(params["id"]);
    });
  }

  submitForm(zipcode: string) {
    this.weatheservice.get(zipcode).subscribe(weatherReq => (this.weatherReq = weatherReq.main));
    console.log(this.weatheservice.get);
  }
}
