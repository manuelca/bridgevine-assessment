import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { CoreModule } from "@app/core";
import { SharedModule } from "@app/shared";
import { weatherRoutingModule } from "./weather-routing.module";
import { WeatherComponent } from "./weather.component";
// import { QuoteService } from './quote.service';
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, FormsModule, SharedModule, weatherRoutingModule],
  declarations: [WeatherComponent],
  providers: []
})
export class WeatherModule {}
