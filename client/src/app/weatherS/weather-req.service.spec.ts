import { TestBed } from '@angular/core/testing';

import { WeatherReqService } from './weather-req.service';

describe('WeatherReqService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeatherReqService = TestBed.get(WeatherReqService);
    expect(service).toBeTruthy();
  });
});
