import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/Rx";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";
@Injectable({
  providedIn: "root"
})
export class WeatherReqService {
  constructor(private http: Http) {}

  get(zipcode: string) {
    return this.http.get(`${environment.BASE_URL}/weather/${zipcode}`).pipe(map(res => res.json()));
  }
}
